#!/usr/bin/env bash

# Run logging with sample input
# -----------------------------
#
# In this case, we'll stream sample input with a bit of a delay for fun.
# This is mostly for development purposes.
set -eo pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
FILE="${1:-"${DIR}/tests/fixtures/access-log.csv"}"

ask() {
    # https://djm.me/ask
    local prompt default reply

    if [ "${2:-}" = "Y" ]; then
        prompt="Y/n"
        default=Y
    elif [ "${2:-}" = "N" ]; then
        prompt="y/N"
        default=N
    else
        prompt="y/n"
        default=
    fi

    while true; do

        # Ask the question (not using "read -p" as it uses stderr not stdout)
        echo -n "$1 [$prompt] "

        # Read the answer (use /dev/tty in case stdin is redirected from somewhere else)
        read reply </dev/tty

        # Default?
        if [ -z "$reply" ]; then
            reply=$default
        fi

        # Check if the reply is valid
        case "$reply" in
            Y*|y*) return 0 ;;
            N*|n*) return 1 ;;
        esac

    done
}

die() {
    printf '%b\n:(\n' "$@"
    exit 1
}

warn() {
    printf "[$(tput setaf 1)WARNING$(tput sgr0)] %b\n" "$@"
	if ! ask 'Continue anyway?' Y; then
		die "User exit."
	fi
}

test -n "$VIRTUAL_ENV" || warn 'not in a virtual environment...'
command -v lumbering &>/dev/null || die 'lumbering not found; try: pip install -e .'

[ ! -r "$FILE" ] && die "Can't read input file: '${FILE}'"

while read -r csv; do
    printf '%b\n' "$csv"
    sleep 0.01
done < "$FILE" | \
    lumbering --csv -
