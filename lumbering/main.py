#!/usr/bin/env python3
"""
lumbering.main
==============

Main loop for the lumbering CLI.
"""

import argparse
import logging
import sys

from blessed import Terminal

from .alarm import Alarm
from .logline import parse
from .stats import Stats
from .stream import stream

TERM = Terminal()
LOG = logging.getLogger('lumbering')


def reset_term():
    """
    Reset terminal
    :return str:
    """
    return TERM.home + TERM.clear


def parse_args(argv):
    """
    Parse main program args
    """
    parser = argparse.ArgumentParser(
        description='CSV-formatted HTTP log insight tool.'
    )
    parser.add_argument(
        '-c',
        '--csv',
        help=('CSV file source. "-" is interpreted as stdin. '
              'Default is stdin.'),
        metavar='<CSV_PATH>',
        default='-'
    )
    parser.add_argument(
        '-v',
        '--verbose',
        help='Show debug output',
        action='store_true',
        dest='debug'
    )
    parser.add_argument(
        '-i',
        '--interval',
        help=('Interval for bucketing request counts in output in seconds. '
              'Defaults to bucket size of 10 seconds.'),
        type=int,
        default=10,
        metavar='<SECONDS>',
        dest='window_size'
    )
    parser.add_argument(
        '--alert-window',
        help=('Size of alert window in seconds. If the average request rate '
              'for this window exceeds the alert-threshold an alarm '
              'is triggered. Defaults to 120 seconds.'),
        type=int,
        default=120,
        metavar='<SECONDS>',
    )
    parser.add_argument(
        '--alert-threshold',
        help=('Threshold for the alert window. If the average request rate '
              'for this window exceeds this count then an alarm is triggered. '
              'Defaults to 10.'),
        type=int,
        default=10,
        metavar='<AVERAGE>'
    )

    return parser.parse_args(argv)


def setup_logging(debug=False):
    """
    Setup logging
    """
    level = logging.INFO

    if debug:
        level = logging.DEBUG

    logging.basicConfig(
        stream=sys.stderr,
        level=level,
        format=('%(asctime)s [%(levelname)s] %(message)s')
    )


def main(argv=None):
    """
    Main program entrypoint
    """
    args = parse_args(argv)
    setup_logging(args.debug)

    # TODO: do I really need logging for this program?
    LOG.debug('Reading from: %s', args.csv)

    log_stats = Stats(args.window_size)
    alarm = Alarm(args.window_size, args.alert_window, args.alert_threshold)

    for line in stream(args.csv):
        print(reset_term())

        log = parse(line)
        if not log:
            continue

        log_stats.append(log)
        print(str(log_stats))

        with TERM.location(0, 0):
            print(TERM.white_on_red + alarm.message(log, log_stats))


if __name__ == '__main__':
    main()
