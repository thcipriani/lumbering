#!/usr/bin/env python
"""
lumbering.stats
===============

This is the core logic for counting/bucketing stats from log lines.
"""
import collections

from .util import iso8601_from_epoch


class Stats(object):
    """
    Collect stats about a series of HTTP requests
    """
    def __init__(self, window_size=10):
        """
        Initialize all stats counters

        :params window_size: int -- size of window into which we collect
                                    request counts
        """
        self.window_size = window_size
        self.requests = collections.Counter()
        self.resources = collections.Counter()
        self.ips = collections.Counter()
        self.methods = collections.Counter()
        self.errors = 0
        self.bytes = 0

    def append(self, log):
        """
        Proccess a new log line.

        :params log: LogLine-type object
        """
        # Cram the request counts into a time window
        window = log.epoch // self.window_size * self.window_size
        self.requests[window] += 1

        # Everything else: just count aggregates
        self.ips[log.src] += 1
        self.methods[log.method] += 1
        self.resources[log.resource] += 1
        if log.is_error:
            self.errors += 1

        self.bytes += log.bytes

    def average_requests_over(self, seconds):
        """
        Get the average request count for the last ``n`` seconds

        :params seconds: number of seconds over which to average
        """
        if seconds < self.window_size:
            raise RuntimeError('Cannot average for fraction of window size')

        # Find the number of buckets that make up the seconds requested
        num_buckets = seconds // self.window_size

        # Find the last n bucket keys
        last_n_keys = list(self.requests.keys())[::-1][:num_buckets]

        total_requests = sum([
            self.requests[x] for x in last_n_keys
        ])

        # report the average number of requests
        total_seconds = len(last_n_keys) * self.window_size
        return total_requests // total_seconds

    def __repr__(self):
        """
        String representation of Stats

        This is what gets ouput the user, so it should be v.fancy :)

        :returns: str
        """
        # TODO: method is too long; much gross-ness
        output = []

        for window_name, count in sorted(
                self.requests.items(), key=lambda x: x[0]):
            timestamp = iso8601_from_epoch(window_name)
            output.append('{}:\t{}'.format(timestamp, count))

        output.append('Errors: {}'.format(self.errors))

        method_output = []
        for method, count in self.methods.most_common():
            method_output.append('{}\t{}'.format(method, count))
        output += ['Methods:\t' + ' | '.join(method_output)]

        ip_output = []
        for ip, count in self.ips.most_common(3):
            ip_output.append('{}:\t{}'.format(ip, count))
        output += ['Top 3 IPs:\t' + ' | '.join(ip_output)]

        resource_output = []
        for resource, count in self.resources.most_common(3):
            resource_output.append('{}\t{}'.format(resource, count))
        output += ['Top 3 Resources:\t' + ' | '.join(resource_output)]

        output.append('Bytes: {}'.format(self.bytes))

        return '\n'.join(output)
