#!/usr/bin/env python3
"""
lumbering.logline
=================

Parse lines of csv input into easily parsable output. See the CSV
formal(-ish) definition in `rfc4180 <https://tools.ietf.org/rfc/rfc4180.txt>`_.
"""
import re

from .util import iso8601_from_epoch


__all__ = ('LogLine', 'parse')


class LogLine(object):
    """
    LogLine object

    Allows easy access to logs.
    """
    def __init__(self, log: list):
        self.src = log[0]
        self.log_time = int(log[3])
        self.method, self.resource = self._parse_req(log[4])
        self.status = int(log[5])
        self.bytes = int(log[6])

    def _parse_req(self, request):
        """
        Parses request string of Apache logs

        :params request: str - "GET /api/user HTTP/1.0"
        """
        methods = [
            'GET',
            'HEAD',
            'POST',
            'PUT',
            'DELETE',
            'CONNECT',
            'OPTIONS',
            'TRACE',
            'PATCH',
        ]

        # TODO: moar error checking!
        # TODO: might be nice to capture HTTP version as well
        method = r'^(?P<method>({}))\W+'.format('|'.join(methods))
        resource = r'(?P<resource>\/[^\/ ]+)\W+'

        pattern = re.compile(method + resource)
        match = pattern.search(request)

        return (match.group('method'), match.group('resource'))

    @property
    def epoch(self):
        """
        Convenience property to get the epoch-formated timestamp
        """
        return self.log_time

    @property
    def timestamp(self):
        """
        Convenience property to get the ISO 8601 formatted timestamp

        See also: `XKCD ISO 8601 <https://xkcd.com/1179/>`_
        """
        return iso8601_from_epoch(self.log_time)

    @property
    def is_error(self):
        """
        Was this a 5xx response?
        """
        return self.status != self.status % 500


def is_header(line: str) -> bool:
    """
    Lame method that just checks against one static header

    :param line: str - possible header
    """
    # TODO: use csv lib to actually check the header line
    # TODO: use header to help parsing
    return line.strip() == ('"remotehost","rfc931","authuser","date",'
                            '"request","status","bytes"')


def parse(line):
    """
    Split on commas, remove quotes.
    """
    if is_header(line):
        return []

    # TODO: use csv sniff to find delimiters
    return LogLine([x.strip('"') for x in line.strip().split(',') if x])
