#!/usr/bin/env python3
"""
lumbering.alarm
===============

Handles alerting logic for the lumbering CLI.
"""


class Alarm(object):
    """
    Checks logs against thresholds
    """

    MSG_FMT = ('High traffic generated on alert - '
               'hits = {value}, triggered at {time}')

    def __init__(self, window_size, alert_window, alert_threshold):
        """
        Initialize alarm object
        """
        self.window = alert_window // window_size
        self.alert_threshold = alert_threshold

    def message(self, log, log_stats) -> str:
        """
        Return an alarm message. If requests are under threshold: return ''

        :param log: LogLine-like object
        :param log_stats: lumbering.Stats-like object
        :return str:
        """
        average = log_stats.average_requests_over(self.window)
        if average < self.alert_threshold:
            return ''

        return self.MSG_FMT.format(value=average, time=log.timestamp)
