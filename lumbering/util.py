#!/usr/bin/env python3
"""
lumbering.util
===============

utility functions. I imagine this is a file that may end up collecting some
cruft as requirements change.
"""
import datetime


def datetime_from_timestamp(t: str) -> str:
    """
    datetime object from timestamp

    :arg t: timestamp string
    :returns str:
    """
    return datetime.datetime.fromtimestamp(t)


def iso8601_from_epoch(t: str) -> str:
    """
    iso8601 format string from timestamp
    """
    return datetime_from_timestamp(t).isoformat()
