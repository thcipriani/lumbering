#!/usr/bin/env python3
"""
lumbering.stream
=========

Stream files
"""
import sys

STDIN = '-'


def iter_file(filename):
    """
    Stream a file
    """
    with open(filename) as filehandle:
        for line in filehandle.read().splitlines():
            yield line.strip()


def iter_stdin():
    """
    Stream standard input
    """
    for line in sys.stdin:
        yield line.strip()


# TODO: this file needs more testing
#       this requires doing some mocking -- sooo lazy
def stream(filename):
    """
    Convenience method for line-based streming iterator
    """
    return iter_stdin() if filename == STDIN else iter_file(filename)
