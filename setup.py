#!/usr/bin/env python3
"""
Lumbering
=========

Package Configuration
"""

from setuptools import find_packages, setup

install_requires = [
    'blessed'
]

tests_require = [
    'coverage',
    'pytest',
]

setup(
    name='lumbering',
    maintainer='Tyler Cipriani',
    maintainer_email='tyler@tylercipriani.com',
    description='CSV-formatted HTTP log insight tool.',
    url='https://gitlab.com/thcipriani/lumbering',
    license='GPLv3+',
    packages=find_packages(exclude=['tests.*']),
    classifiers=[
        'Programming Language :: Python :: 3 :: Only',
        ('License :: OSI Approved :: '
         'GNU General Public License v3 or later (GPLv3+)'),
        'Operating System :: POSIX :: Linux',
        'Intended Audience :: System Administrators',
        'Topic :: System :: Logging',
        'Topic :: Utilities',
    ],
    keywords=['csv', 'http', 'logs', 'apache'],
    install_requires=install_requires,
    entry_points={
        'console_scripts': [
            'lumbering = lumbering.main:main',
        ],
    },
    tests_require=tests_require,
    extras_require={
        'tests': install_requires + tests_require,
    }
)
