CONTRIBUTING TO lumbering
=========================

To contribute a fix or change to lumbering you should include tests.

TESTS
-----

Requirements:

* `tox`

Tox will create a virtualenvironment and install the remaining test
requirements (`flake8`, `pytest` and `coverage`).

Running tests and linting via tox should be done by invoking tox on the command
line:

    $ tox
