README for Lumbering
====================

[Lumbering][] is an HTTP log monitoring console program. It takes a CSV-encoded
HTTP access log and prints insights about the log on stdout. It can accept the
CSV as a stream or as a file parameter.

[Lumbering]: <https://gitlab.com/thcipriani/lumbering>


HERE BE DRAGONS
---------------

I built this over a couple of couple-hour-long sessions one weekend. There are
probably still has some rough-edges that may bork your terminal: you have been
warned.


EXAMPLE
-------

    $ < http-access.log.csv | lumbering --csv -

Unless you're actually tailing a live csv that is being appended to, it may be
clearer to see the output using the `run-lumbering.sh` in the install
directory:

    $ ./run-lumbering.sh http-access.log.csv

![Lumbering run output gif](http://tyler.zone/2020-02-16T14:14:26-07:00.gif)


USAGE
-----

    usage: lumbering [-h] [-c <CSV_PATH>] [-v] [-i <SECONDS>]
                     [--alert-window <SECONDS>] [--alert-threshold <AVERAGE>]

    CSV-formatted HTTP log insight tool.

    optional arguments:
      -h, --help            show this help message and exit
      -c <CSV_PATH>, --csv <CSV_PATH>
                            CSV file source. "-" is interpreted as stdin. Default
                            is stdin.
      -v, --verbose         Show debug output
      -i <SECONDS>, --interval <SECONDS>
                            Interval for bucketing request counts in output in
                            seconds. Defaults to bucket size of 10 seconds.
      --alert-window <SECONDS>
                            Size of alert window in seconds. If the average
                            request rate for this window exceeds the alert-
                            threshold an alarm is triggered. Defaults to 120
                            seconds.
      --alert-threshold <AVERAGE>
                            Threshold for the alert window. If the average request
                            rate for this window exceeds this count then an alarm
                            is triggered. Defaults to 10.


INSTALL
-------

Requirements:

* Python3
* `python3-blessed`

Installation via pip is easy:

    $ git clone https://gitlab.com/thcipriani/lumbering.git
    $ cd lumbering
    $ pip3 install --user .

You can also use a virutalenv -- provided you have virtualenv installed:

    $ git clone https://gitlab.com/thcipriani/lumbering.git
    $ cd lumbering
    $ virtualenv -p python3 venv
    $ . ./venv/bin/activate
    $ pip install -e .


RUNNING TESTS
-------------

See [CONTRIBUTING][]

[CONTRIBUTING]: CONTRIBUTING.md
