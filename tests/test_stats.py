#!/usr/bin/env python3
"""
Lumbering Stats Tests
=====================

Tests for stats
"""

import pytest

import lumbering.stats


class LogTestDouble(object):
    def __init__(
            self,
            epoch=0,
            src='127.0.0.1',
            method='GET',
            resource='/index',
            error=False,
            num_bytes=0):
        self.epoch = epoch
        self.src = src
        self.method = method
        self.resource = resource
        self.is_error = error
        self.bytes = num_bytes


def test_stringification():
    ltd = LogTestDouble(error=True)
    stats = lumbering.stats.Stats()
    stats.append(ltd)
    stats_str = str(stats)
    assert '/index' in stats_str
    assert '127.0.0.1' in stats_str
    assert 'GET' in stats_str
    assert 'Errors: 1' in stats_str


if __name__ == '__main__':
    pytest.main()
