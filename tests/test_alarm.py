#!/usr/bin/env python3
"""
Lumbering Alarm Tests
=====================

Tests for lumbering.alarm.Alarm
"""

import pytest

from lumbering.alarm import Alarm


class DoubleTimestamp(object):
    """
    Test double for an object with a timestamp property
    """
    def __init__(self, ts='1986-08-14T21:00:00'):
        """
        Default to my birthday!
        """
        self.timestamp = ts


class DoubleAverage(object):
    """
    Test double for an object with an average_requests_over property
    """
    def __init__(self, avg=0):
        self.avg = avg
    def average_requests_over(self, n=0):
        return self.avg


def test_alert():
    """Should have an alarm, since 10 < infinity"""
    ts = DoubleTimestamp()
    ta = DoubleAverage(float('inf'))

    alarm = Alarm(window_size=10, alert_window=120, alert_threshold=10)

    assert alarm.message(ts, ta) != ''


def test_no_alert():
    """Should not an alarm, since 10 > 0"""
    ts = DoubleTimestamp()
    ta = DoubleAverage(0)

    alarm = Alarm(window_size=10, alert_window=120, alert_threshold=10)
    assert alarm.message(ts, ta) == ''


if __name__ == '__main__':
    pytest.main()
