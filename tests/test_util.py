#!/usr/bin/env python3
"""
Lumbering Tests
===============

Tests for utils
"""

import pytest


import lumbering.util


def test_iso8601_from_epoch():
    """
    Test for iso8601_from_epoch
    """
    assert isinstance(lumbering.util.iso8601_from_epoch(0), str)


if __name__ == '__main__':
    pytest.main()
