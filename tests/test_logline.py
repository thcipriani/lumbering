#!/usr/bin/env python3
"""
Lumbering LogLine Tests
=======================

Tests for logline
"""

import pytest


from lumbering.logline import *

LOGLINE = parse(
    '"10.0.0.2","-","apache",1549573860,"GET /api/user HTTP/1.0",200,1234'
)


def test_log_line():
    ll = LogLine([
        '10.0.0.2',
        '-',
        'apache',
        '1549573860',
        'GET /api/user HTTP/1.0',
        '200',
        '1234'
    ])
    assert isinstance(ll, LogLine)


def test_parse():
    """Ensure parse returns a logline instance"""
    assert isinstance(LOGLINE, LogLine)
    assert LOGLINE.epoch == 1549573860


def test_method():
    """Ensure that method parsing regex works"""
    assert LOGLINE.method == 'GET'
    assert LOGLINE.resource == '/api'


def test_is_error():
    """Ensure that we can detect errors"""
    ll = LogLine([
        '10.0.0.2',
        '-',
        'apache',
        '1549573860',
        'GET /api/user HTTP/1.0',
        '200',
        '1234'
    ])
    assert not ll.is_error
    ll.status = 404
    assert not ll.is_error
    ll.status = 503
    assert ll.is_error


if __name__ == '__main__':
    pytest.main()
