#!/usr/bin/env python3
"""
Lumbering Tests
===============

Tests for main entrypoint
"""

import pytest

import lumbering.main


def test_reset_term():
    reset = lumbering.main.reset_term()
    # Since the test env is not a tty
    assert len(reset) == 0


def test_parse_args():
    args = lumbering.main.parse_args(['-v'])
    assert args.debug
    assert args.csv == '-'


def test_different_window_size():
    args = lumbering.main.parse_args([])
    assert args.window_size == 10


if __name__ == '__main__':
    pytest.main()
